import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import makePatientsRoutes from "./src/routes/patients.routes";

export default () => {
  const app = express();

  const corsOptions = {
    origin: /http\:\/\/localhost(\:\d+)?/,
    credentials: true,
  };

  app.use(cors(corsOptions));
  app.use(bodyParser.json());

  app.get("/", (req, res) => {
    res.json({ message: "Welcome to patient registry server" });
  });

  makePatientsRoutes(app);

  return app;
};
