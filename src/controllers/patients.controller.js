import db from "../models";

const Patient = db.patients;

const prepareErrors = (data) => {
  return data.map((item) => item.message);
};

export const create = async (req, res) => {
  const patient = {
    full_name: req.body.full_name,
    gender: req.body.gender,
    birthday: req.body.birthday,
    policy_number: req.body.policy_number,
    address: req.body.address,
  };

  try {
    const data = await Patient.create(patient);
    res.send(data);
  } catch (e) {
    res.status(400).send({
      errors: prepareErrors(e.errors),
      message: e.message || "Some error occurred while creating the Patient.",
    });
  }
};

export const findAll = async (req, res) => {
  try {
    const data = await Patient.findAll({ where: null });
    res.send(data);
  } catch (e) {
    res.status(400).send({
      errors: prepareErrors(e.errors),
      message: e.message || "Some error occurred while retrieving patient.",
    });
  }
};

export const findOne = async (req, res) => {
  const id = req.params.id;

  try {
    const data = await Patient.findByPk(id);
    res.send(data);
  } catch (e) {
    res.status(400).send({
      errors: prepareErrors(e.errors),
      message: `Error retrieving Tutorial with id = ${id}`,
    });
  }
};

export const update = async (req, res) => {
  const id = req.params.id;

  try {
    const num = await Patient.update(req.body, {
      where: { id: id },
    });

    if (num == 1) {
      res.send({
        message: "Patient was updated successfully.",
      });
    } else {
      res.send({
        message: `Cannot update Patient with id=${id}. Maybe Patient was not found or req.body is empty!`,
      });
    }
  } catch (e) {
    res.status(400).send({
      errors: prepareErrors(e.errors),
      message: `Error updating Patient with id = ${id}`,
    });
  }
};

export const remove = async (req, res) => {
  const id = req.params.id;

  try {
    const num = await Patient.destroy({ where: { id: id } });

    if (num == 1) {
      res.send({
        message: "Patient was deleted successfully!",
      });
    } else {
      res.send({
        message: `Cannot delete Patient with id=${id}. Maybe Patient was not found!`,
      });
    }
  } catch (e) {
    res.status(400).send({
      errors: prepareErrors(e.errors),
      message: `Could not delete Patient with id = ${id}`,
    });
  }
};
