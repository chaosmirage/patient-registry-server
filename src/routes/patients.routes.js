import { Router } from "express";
import {
  create,
  findAll,
  findOne,
  update,
  remove,
} from "../controllers/patients.controller.js";

export default (app) => {
  const router = Router();

  router.post("/", create);

  router.get("/", findAll);

  router.get("/:id", findOne);

  router.put("/:id", update);

  router.delete("/:id", remove);

  app.use('/api/patients', router);
};
