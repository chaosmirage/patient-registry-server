export default (sequelize, Sequelize) => {
  const Patients = sequelize.define("patients", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      validate: {
        notEmpty: true,
      },
    },
    full_name: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    birthday: {
      type: Sequelize.DATEONLY,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    address: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true,
      },
    },
    policy_number: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [16, 16],
      },
    },
    gender: Sequelize.ENUM({
      values: ["f", "m"],
    }),
  });

  return Patients;
};
