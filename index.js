import _ from "lodash";
import db from "./src/models";
import makeServer from "./server.js";

export default () => {
  db.sequelize.sync();

  const server = makeServer();

  return server;
};
