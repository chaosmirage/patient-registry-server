import request from "supertest";
import server from "../index.js";

const allPatientsFixture = [
  {
    id: 1,
    full_name: "Ivanov Ivan Ivanovich",
    birthday: "1993-01-01",
    address: "st. Pushkin, house 5, apartment 10",
    policy_number: "2234567891234567",
    gender: "f",
    createdAt: "2020-10-08T14:08:29.136Z",
    updatedAt: "2020-10-08T14:08:29.136Z",
  },
];

jest.mock("../src/models/patients.model", () => () => {
  const SequelizeMock = require("sequelize-mock");
  const dbMock = new SequelizeMock();
  return dbMock.define("patients", {
    id: 1,
    full_name: "Ivanov Ivan Ivanovich",
    birthday: "1993-01-01",
    address: "st. Pushkin, house 5, apartment 10",
    policy_number: "2234567891234567",
    gender: "f",
    createdAt: "2020-10-08T14:08:29.136Z",
    updatedAt: "2020-10-08T14:08:29.136Z",
  });
});

it("/", async () => {
  const res = await request(server()).get("/");

  expect(res.text).toStrictEqual(
    JSON.stringify({
      message: "Welcome to patient registry server",
    })
  );
});

it("Get all patients", async () => {
  const res = await request(server()).get("/api/patients");
  expect(res.text).toStrictEqual(JSON.stringify(allPatientsFixture));
});
