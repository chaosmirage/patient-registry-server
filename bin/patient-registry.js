#!/usr/bin/env babel-node

import run from "../index.js";

const port = 3001;

const server = run();

server.listen(port, () => {
  console.log(`Server was started on '${port}'`);
});
