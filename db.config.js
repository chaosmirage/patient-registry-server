export default {
  HOST: process.env.DB_HOST || "localhost",
  USER: process.env.POSTGRES_USER || "adel",
  PASSWORD: process.env.PASSWORD || "",
  DB: process.env.POSTGRES_DB || "patient-registry",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};
